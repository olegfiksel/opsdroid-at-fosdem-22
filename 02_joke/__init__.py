#import random

from opsdroid.matchers import match_regex
from opsdroid.skill import Skill

import os
import subprocess,sys

# import logging
# _LOGGER = logging.getLogger(__name__)

class Fortune(Skill):
    def __init__(self, opsdroid, config):
      super(Fortune, self).__init__(opsdroid, config)
      # Load custom configuration part
      self.conf = self.opsdroid.config['skills']['fortune']['config']
      # Install fortune via pip
      # This is not the best solution, I know ;)
      subprocess.check_call([sys.executable, "-m", "pip", "install", 'fortune'])

    # TODO: rewrite using async
    def _exec_command(self, cmd):
      p = os.popen(str(cmd))
      return str(p.read())

    @match_regex(r'joke', case_sensitive=False, matching_condition='search')
    async def joke(self, message):
        answer = self._exec_command(self.conf['cmd'])
        await message.respond(answer)
