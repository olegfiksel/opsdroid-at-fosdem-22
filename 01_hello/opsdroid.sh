docker run --rm -ti \
	-v `pwd`/configuration.yaml:/etc/opsdroid/configuration.yaml \
	-v `pwd`/__init__.py:/skills/hello/__init__.py \
	-v `pwd`/../e2ee-keys:/home/opsdroid/e2ee-keys \
	--env-file ../secrets.env \
	opsdroid/opsdroid:v0.25.0
