docker run --rm -ti -p 5005:5005 \
  --name rasa \
  rasa/rasa:2.8.19-full \
  run --enable-api --auth-token very-secure-rasa-auth-token -vv
