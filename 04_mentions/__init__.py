#import random

from opsdroid.matchers import match_regex
from opsdroid.skill import Skill

import os
import subprocess,sys
import asyncio
import regex

import logging
_LOGGER = logging.getLogger(__name__)

class Fortune(Skill):
    def __init__(self, opsdroid, config):
      super(Fortune, self).__init__(opsdroid, config)
      # Load custom configuration part
      self.conf = self.opsdroid.config['skills']['fortune']['config']

      # Get only the first part of the mxid
      # like only "bot01" from @bot01:fiksel.info
      match = regex.match(r"\@([^:]+):\w+", self.conf['mxid'])
      if match and match.group(1):
        mxid_user_part = match.group(1)
        self.conf['mention_re_obj'] = regex.compile(mxid_user_part)
        _LOGGER.info("Using \"{}\" for mentions matching".format(mxid_user_part))
      else:
        _LOGGER.error("Cannot find username part in {}".format(self.conf['mxid']))
      
      # Install fortune via pip
      # This is not the best solution, I know ;)
      subprocess.check_call([sys.executable, "-m", "pip", "install", 'fortune'])

    # TODO: rewrite using async
    def _exec_command(self, cmd):
      p = os.popen(str(cmd))
      return str(p.read())

    def _if_mentioned(self, message):
      if self.conf['mention_re_obj'].search(message.text):
          return True
      return False

    @match_regex(r'joke', case_sensitive=False, matching_condition='search')
    async def i_know_jokes(self, message):
        await message.respond("I know some jokes, BTW. \U0001f609")

    @match_regex(r'tell.*joke', case_sensitive=False, matching_condition='search')
    async def tell_joke(self, message):
        if self._if_mentioned(message):
            answer = self._exec_command(self.conf['cmd_joke'])
            await message.respond("{} \U0001f601".format(answer))

    @match_regex(r'cookie', case_sensitive=False, matching_condition='search')
    async def i_can_open_cookie(self, message):
        await message.respond("I can open a fortune cookie for you. \U0001f609")

    @match_regex(r'open.*cookie', case_sensitive=False, matching_condition='search')
    async def open_cookie(self, message):
        if self._if_mentioned(message):
            await message.respond("Sure! Opening the cookie... ")
            await asyncio.sleep(3)
            await message.respond("Yum,yum \U0001f60b")
            await asyncio.sleep(2)
            cookie_text = self._exec_command(self.conf['cmd_cookie'])
            await message.respond("The fortune cookie text says: "+cookie_text)

    @match_regex(r'you.*robot', case_sensitive=False, matching_condition='search')
    async def are_you_robot(self, message):
        if self._if_mentioned(message):
            await message.respond("I don't know, are you?")