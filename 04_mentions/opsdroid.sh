docker run --rm -ti \
	-v `pwd`/configuration.yaml:/etc/opsdroid/configuration.yaml \
	-v `pwd`/../e2ee-keys:/home/opsdroid/e2ee-keys \
	-v `pwd`:/skills/fortune \
	--env-file ../secrets.env \
	opsdroid/opsdroid:v0.25.0
