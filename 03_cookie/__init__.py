#import random

from opsdroid.matchers import match_regex
from opsdroid.skill import Skill

import os
import subprocess,sys
import asyncio

# import logging
# _LOGGER = logging.getLogger(__name__)

class Fortune(Skill):
    def __init__(self, opsdroid, config):
      super(Fortune, self).__init__(opsdroid, config)
      # Load custom configuration part
      self.conf = self.opsdroid.config['skills']['fortune']['config']
      # Install fortune via pip
      # This is not the best solution, I know ;)
      subprocess.check_call([sys.executable, "-m", "pip", "install", 'fortune'])

    # TODO: rewrite using async
    def _exec_command(self, cmd):
      p = os.popen(str(cmd))
      return str(p.read())

    @match_regex(r'joke', case_sensitive=False, matching_condition='search')
    async def joke(self, message):
        answer = self._exec_command(self.conf['cmd_joke'])
        await message.respond(answer)

    @match_regex(r'cookie', case_sensitive=False, matching_condition='search')
    async def cookie(self, message):
        await message.respond("Sure! Opening the cookie... ")
        await asyncio.sleep(3)
        await message.respond("Yum,yum \U0001f60b")
        await asyncio.sleep(2)
        cookie_text = self._exec_command(self.conf['cmd_cookie'])
        await message.respond("The fortune cookie text says: "+cookie_text)